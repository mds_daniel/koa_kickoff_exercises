const Koa = require('koa');

const app = new Koa();

app.use(async (ctx, next) => {
  if (ctx.request.is('application/json')) {
    ctx.response.body = { message: 'hi!' };
    ctx.response.type = 'application/json';
  } else {
    ctx.response.body = 'ok';
  }
});

const port = process.argv[2];
app.listen(port);

