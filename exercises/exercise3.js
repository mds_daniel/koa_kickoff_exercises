const Koa = require('koa');
const Router = require('@koa/router');
const parse = require('co-body');

const app = new Koa();
const router = new Router();
const port = process.argv[2];

router.post('/', async(ctx, next) => {
  const body = await parse(ctx, { limit: '1kb' });
  const { name } = body;
  ctx.assert(typeof name === 'string', 422)

  ctx.response.body = name.toUpperCase();
  ctx.response.status = 200;

  await next();
});

router.get('/', async (ctx, next) => {
  ctx.response.body = 'hello koa';
  await next();
});

router.get('/404', async (ctx, next) => {
  ctx.response.body = 'page not found';
  ctx.response.status = 404;

  await next();
});

router.get('/500', async (ctx, next) => {
  ctx.response.body = 'internal server error';
  ctx.response.status = 500;

  await next();
});

app.use(router.routes())
   .use(router.allowedMethods());

app.listen(port);
