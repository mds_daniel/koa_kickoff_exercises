const Koa = require('koa');
const { errorHandler } = require('./helpers.js');

const app = new Koa();
const port = process.argv[2];

app.use(errorHandler());

app.use(async function(ctx, next) {
  if (ctx.request.path === '/error') {
    throw new Error('ooops');
  }

  ctx.response.body = 'OK';
});

app.listen(port);
