const Koa = require('koa');
const session = require('koa-session');

const app = new Koa();
const port = process.argv[2];
const sessionConfig = {
  key: 'koa:sess',
  signed: true,
};

// Set cookie signing keys
app.keys = [
  'OEK5zjaAMPc3L6iK7PyUjCOziUH3rsrMKB9u8H07La1SkfwtuBoDnHaaPCkG5Brg',
  'MNKeIebviQnCPo38ufHcSfw3FFv8EtnAe1xE02xkN1wkCV1B2z126U44yk2BQVK7',
];

app.use(session(sessionConfig, app));

app.use(async (ctx, next) => {
  let views = ctx.session.view || 0;
  views = Number(views) + 1;
  ctx.session.view = views;

  ctx.response.body = `${views} views`;

  await next();
});

app.listen(port);
