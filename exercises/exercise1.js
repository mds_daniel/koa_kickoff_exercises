const Koa = require('koa');
const app = new Koa();
const port = process.argv[2];

app.use(async (ctx, next) => {
  ctx.body = 'hello koa';
  await next();
});

app.listen(port);
