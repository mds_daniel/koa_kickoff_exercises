const Koa = require('koa');
const Router = require('@koa/router');

const app = new Koa();
const router = new Router();
const port = process.argv[2];

router.get('/', async (ctx, next) => {
  ctx.response.body = 'hello koa';
  await next();
});

router.get('/404', async (ctx, next) => {
  ctx.response.body = 'page not found';
  await next();
});

router.get('/500', async (ctx, next) => {
  ctx.response.body = 'internal server error';
  await next();
});

app.use(router.routes())
   .use(router.allowedMethods());

app.listen(port);
