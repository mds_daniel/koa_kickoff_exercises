const Koa = require('koa');

const app = new Koa();
const port = process.argv[2];

app.keys = [
  'OEK5zjaAMPc3L6iK7PyUjCOziUH3rsrMKB9u8H07La1SkfwtuBoDnHaaPCkG5Brg',
  'MNKeIebviQnCPo38ufHcSfw3FFv8EtnAe1xE02xkN1wkCV1B2z126U44yk2BQVK7',
];

app.use(async function(ctx, next) {
  let views = incrementViews(ctx);
  ctx.response.body = `${views} views`;

  await next();
});

app.listen(port);

// Helpers
const cookieOpts = { signed: true };

function incrementViews(ctx) {
  let value = ctx.cookies.get('view', cookieOpts) || 0;

  value = Number(value) + 1;
  ctx.cookies.set('view', value, cookieOpts);

  return value;
}
