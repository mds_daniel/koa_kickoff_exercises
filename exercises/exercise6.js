const Koa = require('koa');
const { responseTime, upperCase } = require('./helpers.js');

const app = new Koa();

app.use(responseTime());
app.use(upperCase());
app.use(async (ctx, next) => {
  ctx.response.body = 'hello koa';
});

const port = process.argv[2];
app.listen(port);
