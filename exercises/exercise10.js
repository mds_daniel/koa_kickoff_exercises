const Koa = require('koa');
const views = require('co-views');

const app = new Koa();
const port = process.argv[2];
const user = {
  name: {
    first: 'Tobi',
    last: 'Holowaychuk',
  },
  species: 'ferret',
  age: 3,
};

const render = views(`${__dirname}/views`, { ext: 'ejs' });

app.use(async (ctx, next) => {
  ctx.response.body = await render('user', { user });

  await next();
});

app.listen(port);
