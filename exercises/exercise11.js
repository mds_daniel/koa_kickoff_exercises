const path = require('path');
const Koa = require('koa');
const Router = require('@koa/router');
const koaBody = require('koa-body');
const session = require('koa-session');
const views = require('koa-views');

const app = new Koa();
const router = new Router();
const port = process.argv[2];
const render = views(path.join(__dirname, 'views'), { extension: 'ejs' });

// Set cookie signing keys
app.keys = [
  'OEK5zjaAMPc3L6iK7PyUjCOziUH3rsrMKB9u8H07La1SkfwtuBoDnHaaPCkG5Brg',
  'MNKeIebviQnCPo38ufHcSfw3FFv8EtnAe1xE02xkN1wkCV1B2z126U44yk2BQVK7',
];

// Set routes
router.get('/', async (ctx) => {
  const user = getCurrentUser(ctx.session);

  if (user) {
    ctx.response.body = 'hello world';
  } else {
    ctx.response.status = 401;
    ctx.response.body = 'Unauthenticated';
  }
});

router.get('/login', async (ctx) => {
  ctx.response.body = await ctx.render('form.ejs');
});

router.post('/login', async (ctx) => {
  // validate user credentials, and redirect to `/`
  const { username, password } = ctx.request.body;

  if (validateCredentials({ username, password })) {
    setCurrentUser(ctx.session, { username });
    ctx.response.redirect('/');
  } else {
    ctx.response.status = 400;
    ctx.response.body = 'Invalid login crendetials';
  }
});

router.all('/logout', async (ctx) => {
  // logout user and redirect to login
  ctx.session = null;
  ctx.response.redirect('/login');
});


// Set middleware stack
app.use(session(app));
app.use(koaBody());
app.use(render);
app.use(router.routes())
   .use(router.allowedMethods());

// Start app listing on port
console.log(`Listening on port ${port}`);
app.listen(port);

// Helpers

// currentUser helpers
function setCurrentUser(session, user) {
  session.currentUser = user;
}

function getCurrentUser(session) {
  return session.currentUser;
}

// Mock credential validation
function validateCredentials({ username, password }) {
  return (username === 'username') && (password === 'password');
}
