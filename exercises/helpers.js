function responseTime() {
  return async function(ctx, next) {
    const start = Date.now();

    await next();

    const duration = Date.now() - start;
    ctx.response.set('X-Response-Time', duration);
  }
}

function upperCase() {
  return async function(ctx, next) {
    await next();
    ctx.response.body = ctx.response.body.toUpperCase();
  }
}

function errorHandler() {
  return async function(ctx, next) {
    try {
      await next();
    } catch (err) {
      ctx.response.body = 'internal server error';
      ctx.response.status = 500;
      ctx.app.emit('error', err, ctx);
    }
  }
}

module.exports = {
  responseTime,
  upperCase,
  errorHandler,
};
